# What is this?
I'm a steam trader and I had always wished there's a way to get notified on my mobile phone when somebody sent me trader offers but there's no such mobile app currently, at least for Windows Phone OS. So long story short, this script was born out of these 2 frustrations. On new steam notifications (any kind of notification, not just trader offers), it will send a push notification to your mobile devices using Pushalot service.


# How to use?
1. Copy login_example.conf as login.conf
2. Get steam guard cookie from your web browser (the one with a name like steamMachineAuth{steamid64})
3. Enter your steam login info and the steam guard cookie just now under the steam field in login.conf
4. Run python steam_notification.py (for more options see steam_notification.py -h)


If you have a server or machine that can run python and runs around the clock, you can fire and forget this script on the machine to have it running in the background and alert you on new steam notification. Or you can deploy it on Heroku using free dyno hours. All the deployment files you need are in the Heroku folder, just move them out to root dir and push this project to your heroku repo