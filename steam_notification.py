#!/usr/bin/env python

"""
Steam Notification for Pushalot (v1.0)
Send pushalot notification on new steam notifications

This script is released under the GPL (http://www.gnu.org/licenses/gpl.html)
Copyright 2015 blitzbite

Contact: http://steamcommunity.com/id/blitzbite
"""


import json
import requests
import logging
import os.path
import argparse
from bs4 import BeautifulSoup
from time import time, sleep
from re import match
from sys import argv, exit
from collections import OrderedDict

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5



# TODO:
# - retry sending failed push notification
# - login from console and save login auth data to config


# Credits and references:
# Initial login code by Doldol @ eventscripts forum
#   - http://forums.eventscripts.com/viewtopic.php?f=90&t=46941
# steamommunity.com login routine
#   - https://steamcommunity.com/public/javascript/login.js



notification_items_string = {
    'comments': 'Comments',
    'items': 'Items',
    'invites': 'Invites',
    'gifts': 'Gifts',
    'offlinemessages': 'Offline Messages',
    'tradeoffers': 'Trade Offers'
}


class SteamExpiredSession(Exception):
    """login session must have expired"""
    pass


class SteamNotification(object):

    def __init__(self, conf, push=True):
        with open(conf, 'r') as f:
            settings = json.load(f)

            try:
                self.username = settings['steam']['username']
                self.password = settings['steam']['password']
                self.steamguard = settings['steam']['steamguard']

                if push:
                    self.auth_token = settings['pushalot']['auth_token']
                    self.silent = settings['pushalot'].get('silent', False)
                    self.important = settings['pushalot'].get('important', True)

                # make sure these keys are set
                if not self.username:
                    raise ValueError('username')
                elif not self.password:
                    raise ValueError('password')
                elif not self.steamguard:
                    raise ValueError('steamguard')

                if push:
                    if not self.auth_token:
                        raise ValueError('auth_token')

            except KeyError as e:
                logger.critical("Option {0} is missing. Please add and set the option".format(e))
                exit(1)
            except ValueError as e:
                logger.critical("Option {0} is not set. Please set the option".format(e))
                exit(1)

        self.send_push = push

        self.rs = requests.Session()
        self.rs.headers.update({'User-Agent': 'Mozilla/5.0 Firefox/37.0'})
        self.rs.cookies.update(self.steamguard)

        self.old_notification = OrderedDict()


    def get_rsakey(self):
        url = 'https://steamcommunity.com/login/getrsakey/'
        params = {'username': self.username, 'donotcache': str(int(time()))}

        r = self.rs.post(url, params=params, timeout=10)
        return r.json()


    def encrypt_password(self, pk_mod, pk_exp, password):
        mod = long(str(pk_mod), 16)
        exp = long(str(pk_exp), 16)
        rsa = RSA.construct((mod, exp))
        cipher = PKCS1_v1_5.new(rsa)

        return cipher.encrypt(password.encode('ascii', 'ignore')).encode('base64').replace('\n', '')


    def try_login_steam(self, epassword, timestamp, **kparams):
        url = 'https://steamcommunity.com/login/dologin/'
        params = {
                'username' : self.username,
                "password": epassword,
                "emailauth": "",
                "loginfriendlyname": "",
                "captchagid": "-1",
                "captcha_text": "",
                "emailsteamid": "",
                "rsatimestamp": timestamp,
                "remember_login": False,
                "donotcache": str(int(time()))
        }
        r = self.rs.post(url, params=params, timeout=10)
        return r.json()


    def collect_notification(self):
        logger.debug("Fetching /actions/RefreshNotificationArea")

        url = 'https://steamcommunity.com/actions/RefreshNotificationArea'
        r = self.rs.get(url, timeout=10)

        logger.debug("Status code: {0}".format(r.status_code))
        if r.status_code == requests.codes.ok:
            html = r.text
            if not len(html):
                logger.debug("Notification data returned is empty (status_code: {0}".format(r.status_code))
                raise SteamExpiredSession("Relogin required")

        notifications = OrderedDict()

        soup = BeautifulSoup(html)
        notification_items = ('comments', 'items', 'invites', 'gifts', 'offlinemessages', 'tradeoffers')
        for item in notification_items:
            element = soup.select('a.popup_menu_item.header_notification_{0}'.format(item))
            if len(element):
                count = match('\d+', element[0].get_text().strip()).group()
                notifications[item] = int(count)
            else:
                notifications[item] = 0

        return notifications


    def send_push_notification(self, message):
        url = 'https://pushalot.com/api/sendmessage'
        params = {
                'AuthorizationToken': self.auth_token,
                'Title': 'Steam Notification',
                'Body': '{0}'.format(message),
                'IsImportant': self.important,
                'IsSilent': self.silent
        }
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        req = requests.post(url, headers=headers, params=params, timeout=10)

        return req.status_code == requests.codes.ok


    def login_steam(self):
        logger.info("Fetching rsa key for login process")
        try:
            data = self.get_rsakey()
        # except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
        except:
            data = None
            logger.error("Unable to fetch rsa key")


        if data:
            if data['success'] == True:
                logger.info("Trying to log into steamcommunity.com")

                epassword = self.encrypt_password(data['publickey_mod'], data['publickey_exp'], self.password)
                try:
                    loginstatus = self.try_login_steam(epassword, data['timestamp'])
                except:
                    loginstatus = None
                    logger.error("Unable to connect to login")

                if loginstatus:
                    if loginstatus['success'] == True:
                        logger.info("Logged into steamcommunity.com successfully")
                        return True
                    else:
                        logger.critical("Failed to log into steamcommunity.com. Login info is incorrect")
                        exit(1)


        return False


    def manage_notification(self, notification):
        all_notif = notification
        new_notif = OrderedDict()
        for k, v in all_notif.iteritems():
            if v > self.old_notification.get(k, 0):
                new_notif[k] = v

        logger.debug("New notification: {0}".format('None' if not len(new_notif) else new_notif))
        if len(new_notif) != 0:
            # format notification messages
            """
            You have new notifications!
              Invites          : 1
              Offline Messages : 2
              Trade Offers     : 5

            All notifications:
              Comments         : 0
              Items            : 0
              Invites          : 1
              Gifts            : 10
              Offline Messages : 2
              Trade Offers     : 5

            """
            logger.info("You have new notifications ({0})".format(sum(new_notif.values())))

            notif_msg_str = 'You have new notifications!\n'
            for k, v in new_notif.iteritems():
                notif_msg_str += '  {0:<17} : {1}\n'.format(notification_items_string[k], v)

            notif_msg_str += '\nAll notifications:\n'
            for k, v in all_notif.iteritems():
                notif_msg_str += '  {0:<17} : {1}\n'.format(notification_items_string[k], v)

            logger.debug('\n' + notif_msg_str)

            if self.send_push:
                logger.info('Sending push notification')
                try:
                    push = self.send_push_notification(notif_msg_str)
                except:
                    push = None
                    logger.error("Failed to send push notification")

                if push:
                    logger.info("Push notification sent successfully")

            self.old_notification = all_notif


    def start_loop(self):
        is_logged_in = False
        sleep_delay = 0

        try:
            while True:
                cycle_completed = False

                if not is_logged_in:
                    is_logged_in = self.login_steam()

                if is_logged_in:
                    try:
                        notification = self.collect_notification()
                    except SteamExpiredSession:
                        logger.warning("Relogin required")
                        is_logged_in = False
                        # continue
                    except:
                        logger.error("Unable to fetch notification data")
                    else:
                        logger.debug(notification)
                        """ This outputs, in order:
                                {'comments': 0,
                                'items': 0,
                                'invites': 0
                                'gifts': 0,
                                'offlinemessages': 0,
                                'tradeoffers': 0}
                        """
                        self.manage_notification(notification)
                        cycle_completed = True


                if not cycle_completed:
                    sleep_delay += 10
                else:
                    sleep_delay = 0

                logger.debug("Sleep ({0})".format(60 + sleep_delay))
                sleep(60 + sleep_delay)

        except KeyboardInterrupt:
            logger.info("Bye!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', help='use custom config (if omitted defaults to login.conf in the script dir)')
    parser.add_argument('--push', choices=['yes', 'no'], default='no', help='send push notification on new steam notification (default: no)')
    parser.add_argument('--log', choices=['debug', 'info', 'warning', 'error', 'critical'], default='info', help='set log level (default is info)')
    args = parser.parse_args()

    loglevel = args.log
    logger = logging.getLogger('SteamNotification')
    logger.setLevel(loglevel.upper())
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(asctime)s: %(levelname)s: %(message)s'))
    logger.addHandler(handler)
    logger.propagate = False

    logger.info("SteamNotification started")
    conf_file = args.config
    if not conf_file:
        logger.warning("No conf file given, falling back to login.conf in the script dir")
        conf_file = os.path.dirname(os.path.abspath(argv[0])) + '/login.conf'
        if not os.path.isfile(conf_file):
            logger.critical("Default config 'login.conf' not found in script dir. Exiting")
            exit(1)
    else:
        if not os.path.isfile(conf_file):
            logger.critical("{0}: No such conf file. Exiting".format(conf_file))
            exit(1)

    send_push = True if args.push.lower() == 'yes' else False
    sn = SteamNotification(conf_file, push=send_push)
    sn.start_loop()

